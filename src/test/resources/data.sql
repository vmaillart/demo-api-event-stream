INSERT INTO kiosk_customer_identification (FIRST_NAME, LAST_NAME)
VALUES
	('Bruce', 'Wayne');

INSERT INTO kiosk_customer_identification (FIRST_NAME, LAST_NAME)
VALUES
	('Tony', 'Stark'),
	('Carol', 'Danvers'),
	('Steve', 'Rogers'),
	('Natasha', 'Romanoff');

INSERT INTO kiosk_customer_identification (FIRST_NAME, LAST_NAME)
VALUES
	('Prénom01', 'Nom01'),
	('Prénom02', 'Nom02'),
	('Prénom03', 'Nom03'),
	('Prénom04', 'Nom04'),
	('Prénom05', 'Nom05'),
	('Prénom07', 'Nom07'),
	('Prénom08', 'Nom08'),
	('Prénom09', 'Nom09'),
	('Prénom10', 'Nom10'),
	('Prénom11', 'Nom11'),
	('Prénom12', 'Nom12'),
	('Prénom13', 'Nom13'),
	('Prénom14', 'Nom14'),
	('Prénom15', 'Nom15'),
	('Prénom16', 'Nom16'),
	('Prénom17', 'Nom17'),
	('Prénom18', 'Nom18'),
	('Prénom19', 'Nom19'),
	('Prénom20', 'Nom20');