package dev.valora.demoapieventstream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@SpringBootApplication
public class DemoApiEventStreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApiEventStreamApplication.class, args);
	}

	@Configuration
	@EnableWebFlux
	class CorsConfiguration implements WebFluxConfigurer {

		@Override
		public void addCorsMappings(CorsRegistry registry) {
			registry.addMapping("/**").allowedOrigins("*");
		}

	}

}
