package dev.valora.demoapieventstream.rest.controller;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.valora.demoapieventstream.database.model.KioskCustomerIdentification;
import dev.valora.demoapieventstream.database.repository.KioskCustomerIdentificationRepository;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/kiosks")
public class KioskController {

	private static final Logger LOG = LoggerFactory.getLogger(KioskController.class);

	@Autowired
	private KioskCustomerIdentificationRepository kioskCustomerIdentificationRepository;

	/**
	 * When a customer come retrieving its order and identify itself with the kiosk, create an `customer-identification` type event
	 * 
	 * @return Events related to kiosk
	 */
	@GetMapping("/events")
	// produces = MediaType.TEXT_EVENT_STREAM_VALUE useless with ServerSentEvent return type
	public Flux<ServerSentEvent<String>> streamEvents(@RequestHeader(name = "Last-Event-ID", required = false) Long lastEventId) {
		LOG.info("lastEventId={}", lastEventId);

		Flux<KioskCustomerIdentification> kioskCustomerIdentifications;
		if (lastEventId == null) {
			kioskCustomerIdentifications = kioskCustomerIdentificationRepository.findAll();
		} else {
			kioskCustomerIdentifications = kioskCustomerIdentificationRepository.findByIdGreaterThan(lastEventId);
		}

		return kioskCustomerIdentifications.delayElements(Duration.ofSeconds(2))
				.map(customer -> ServerSentEvent.<String>builder()
						.id(String.valueOf(customer.getId()))
						.event("customer-identification")
						.data(customer.toString())
						.build());
	}

}
