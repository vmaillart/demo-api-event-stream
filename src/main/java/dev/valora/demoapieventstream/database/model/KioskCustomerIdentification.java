package dev.valora.demoapieventstream.database.model;

import org.springframework.data.annotation.Id;

public class KioskCustomerIdentification {

	@Id
	private Long id;

	private String firstName;

	private String lastName;

	public Long getId() {
		return this.id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	@Override
	public String toString() {
		return String.format("Customer[id=%d, firstName='%s', lastName='%s']", id, firstName, lastName);
	}

}
