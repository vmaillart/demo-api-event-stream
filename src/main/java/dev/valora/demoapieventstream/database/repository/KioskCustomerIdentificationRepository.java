package dev.valora.demoapieventstream.database.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import dev.valora.demoapieventstream.database.model.KioskCustomerIdentification;
import reactor.core.publisher.Flux;

public interface KioskCustomerIdentificationRepository extends ReactiveCrudRepository<KioskCustomerIdentification, Long> {

	Flux<KioskCustomerIdentification> findByIdGreaterThan(Long lastEventId);

}
