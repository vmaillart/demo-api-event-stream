package dev.valora.demoapieventstream.database;

import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationPropertiesScan(basePackageClasses = FlywayProperties.class)
public class FlywayConfiguration {

	@Bean(initMethod = "migrate")
	public Flyway flyway(FlywayProperties flywayProperties) {
		return new Flyway(Flyway.configure()
				.baselineOnMigrate(true)
				.dataSource(flywayProperties.getUrl(), flywayProperties.getUser(), flywayProperties.getPassword()));
	}

}
