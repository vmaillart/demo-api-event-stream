CREATE TABLE `kiosk_customer_identification` (
	`id`			int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`first_name`	varchar(50),
	`last_name`		varchar(50)
)